﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnGUIConsole : MonoBehaviour
{
    enum ConsoleStatus
    {
        Off,
        Console,
        Menu,
    }

    bool _debug = false;
    bool _bindsOn = true;
    ConsoleStatus status = ConsoleStatus.Off;

    string path = "";
    public static OnGUIConsole Instance;
    Rect windowRect = new Rect();
    string _textFieldText = "";
    List<string> _consoleText = new List<string>();
    List<string> _consoleTextTransperent = new List<string>();
    float _consoleTextTransperentTime = 0;
    float _consoleTextTransperentDelay = 3;
    int _maxStrings = 500;
    int _maxVisibleStrings = 10;
    Dictionary<string, string> _binds = new Dictionary<string, string>();
    Dictionary<string, string[]> _aliases = new Dictionary<string, string[]>();
    List<string> _menuList = new List<string>();

    public Action<string> SendFunc = (message) => { };

    private float scrollValue = 0;

    private int stringIndex = 0;

    //string AutoExec = "";

    GUIStyle _nonbreakingLabelStyle;

    private int maxSubmitCallsCount = 1000;

    // Use this for initialization
    void Start()
    {
        _nonbreakingLabelStyle = new GUIStyle();
        _nonbreakingLabelStyle.wordWrap = false;
        _nonbreakingLabelStyle.normal.textColor = Color.white;

        path = Application.dataPath + "/OnGUIConsole/";

        SendFunc = (txt) => ClientRpcListener.Instance.SendSendChatMessageRequest(txt, ChatSection.Private);

        windowRect.Set(0, 0, Screen.width, _maxVisibleStrings * 15.625f + 50);

        if (File.Exists(path + "AutoExec.txt"))
        {
            string[] auto = File.ReadAllLines(path + "AutoExec.txt");
            for (int i = 0; i < auto.Length; i++)
            {
                Submit(auto[i], true, 0);
            }
        }
        if (File.Exists(path + "SavedCommands.txt"))
        {
            string[] auto = File.ReadAllLines(path + "SavedCommands.txt");
            for (int i = 0; i < auto.Length; i++)
            {
                Submit(auto[i], true, 0);
            }
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void OnDisable()
    {
        Save();
    }

    void PrintBinds()
    {
        foreach (var item in _binds)
        {
            Print("[" + item.Key + "]" + " \"" + item.Value + "\"");
        }
    }

    void PrintAliases()
    {
        foreach (var item in _aliases)
        {
            if (item.Value.Length == 0)
                continue;
            string tt = "{" + item.Key + "}";
            foreach (var command in item.Value)
            {
                tt += " \"" + command + "\"";
            }
            Print(tt);
        }
    }

    private void Save()
    {

        List<string> strings = new List<string>();
        foreach (var item in _binds)
        {
            if (item.Value.Contains(" "))
                strings.Add("bind " + item.Key + " \"" + item.Value + "\"");
            else
                strings.Add("bind " + item.Key + " " + item.Value);
        }

        foreach (var item in _aliases)
        {
            if (item.Value.Length == 0)
                continue;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("alias " + item.Key);
            foreach (var arg in item.Value)
            {
                if (arg.Contains(" "))
                    sb.Append(" \"" + arg + "\"");
                else
                    sb.Append(" " + arg);
            }
            strings.Add(sb.ToString());
        }

        File.WriteAllLines(path + "SavedCommands.txt", strings.ToArray());
    }

    // Update is called once per frame
    void Update()
    {
        if (_textFieldText.Contains("\n"))
        {
            Submit(_textFieldText.Replace("\n", ""), false, 0);
        }
        if (_textFieldText.Contains("`"))
        {
            _textFieldText = _textFieldText.Replace("`", "");
            status = ConsoleStatus.Off;
            Save();
        }
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            status = ConsoleStatus.Console;
            Save();
        }

        foreach (var item in _binds)
        {
            if (_bindsOn || item.Value.Contains("binds_on"))
                if (Input.GetKeyDown(item.Key))
                {
                    Submit(item.Value, false, 0);
                }
        }
        if (_consoleTextTransperent.Count > 0 && _consoleTextTransperentTime < Time.time)
        {
            _consoleTextTransperentTime = Time.time + _consoleTextTransperentDelay / (_consoleTextTransperent.Count / 5 + 1);
            _consoleTextTransperent.RemoveAt(0);
        }
        Timer.Update();
    }



    private void OnGUI()
    {
        UpDownKeyControl();
        if (status == ConsoleStatus.Console)
        {
            windowRect = GUI.Window(1, windowRect, ConsoleWindow, "console");
            int j = 0;
            if (!string.IsNullOrEmpty(_textFieldText))
                foreach (var item in _aliases)
                {
                    if (item.Key.Contains(_textFieldText.Split(' ')[0]))
                    {
                        j++;
                        if (GUI.Button(new Rect(windowRect.position.x, windowRect.position.y + windowRect.height - 30 + j * 30, windowRect.width / 2 - 100, 30), item.Key.Replace("*", " ")))
                        {
                            _textFieldText = item.Key.Split('*')[0];
                        }
                    }
                    if (j > 30)
                        break;
                }
        }
        if (status != ConsoleStatus.Console)
        {
            for (int i = 0; i < _consoleTextTransperent.Count; i++)
            {
                GUILayout.Label(_consoleTextTransperent[i], _nonbreakingLabelStyle);

            }
        }
        GUILayout.BeginArea(new Rect(windowRect.position.x + 10, windowRect.height - 30 - 20, Screen.width - 20, Screen.height - 30 - 20));
        /* foreach (var item in _menuList)
         {
             if (GUILayout.Button(item))
             {
                 Submit(item, false);
             }
         }*/
        GUILayout.EndArea();
    }

    private void UpDownKeyControl()
    {
        Event e = Event.current;
        if ((e.keyCode == KeyCode.UpArrow || e.keyCode == KeyCode.PageUp) && e.type == EventType.KeyUp)
        {
            stringIndex++;
            if (stringIndex > _consoleText.Count)
                stringIndex = _consoleText.Count;
            if (stringIndex > 0)
                _textFieldText = _consoleText[_consoleText.Count - stringIndex];
        }
        else if ((e.keyCode == KeyCode.DownArrow || e.keyCode == KeyCode.PageDown) && e.type == EventType.KeyUp)
        {
            stringIndex--;
            if (stringIndex < 0)
                stringIndex = 0;
            if (stringIndex > 0)
                _textFieldText = _consoleText[_consoleText.Count - stringIndex];
            else
                _textFieldText = "";
        }
    }

    void OnEnabled()
    {

    }

    void ConsoleWindow(int windowID)
    {
        GUILayout.BeginArea(new Rect(windowRect.position.x + 10, windowRect.position.y + 20, windowRect.width - 20, windowRect.height - 30 - 20));
        for (int i = Mathf.Max(0, _consoleText.Count - _maxVisibleStrings - (int)scrollValue); i < _maxStrings; i++)
        {
            if (_consoleText.Count > i)
                GUILayout.Label(_consoleText[i], _nonbreakingLabelStyle);
            else
                GUILayout.Label("");
        }
        GUILayout.EndArea();
        scrollValue = GUI.VerticalScrollbar(new Rect(windowRect.width - 20, 10, 50, windowRect.height - 50), scrollValue, 1.0F, Mathf.Max(0, _consoleText.Count - _maxVisibleStrings + 1), 0.0F);

        GUI.SetNextControlName("MyTextField");
        _textFieldText = GUI.TextArea(new Rect(windowRect.position.x, windowRect.position.y + windowRect.height - 30, windowRect.width - 100, 30), _textFieldText);

        if (GUI.Button(new Rect(windowRect.position.x + windowRect.width - 100, windowRect.position.y + windowRect.height - 30, 100, 30), "Submit"))
        {
            Submit(_textFieldText, false, 0);
        }

        GUI.FocusControl("MyTextField");
    }

    void Submit(string text, bool noPrint, int callsCount)
    {
        stringIndex = 0;
        if (callsCount > maxSubmitCallsCount)
        {
            Print("!!!to many calls!!!");
            return;
        }
        if (string.IsNullOrEmpty(text))
            return;
        text = text.Split(new string[] { "//" }, StringSplitOptions.None)[0].Replace("\t", "");
        if (!noPrint || _debug)
            Print(text);
        if (!Execute(LineReader(text), callsCount))
            SendFunc(text);
        _textFieldText = "";
    }

    string[] LineReader(string text)
    {
        List<string> args = new List<string>(new string[] { "" });
        int j = 0;
        bool openQuotes = false;
        for (int i = 0; i < text.Length; i++)
        {
            if (text[i] == '"')
            {
                openQuotes = !openQuotes;
                if (!string.IsNullOrEmpty(args[j]))
                {
                    j++;
                    args.Add("");
                }
                continue;
            }
            if (text[i] == ' ' && !openQuotes)
            {
                if (!string.IsNullOrEmpty(args[j]))
                {
                    j++;
                    args.Add("");
                }
                continue;
            }
            args[j] += text[i];
        }

        if (args[j] == "")
        {
            args.RemoveAt(j);
        }
        if (_debug)
        {
            for (int i = 0; i < args.Count; i++)
            {
                Debug.Log(args[i] + " " + i);
            }
        }
        return args.ToArray();
    }

    bool UnpackAliasToMenuList(string alias)
    {
        string[] args2;
        _menuList.Clear();
        if (_aliases.TryGetValue(alias, out args2))
        {
            for (int i = 0; i < args2.Length; i++)
            {
                _menuList.Add(args2[i]);
            }
            return true;
        }
        return false;
    }

    public bool Execute(string[] args, int callsCount)
    {
        if (args.Length == 0)
            return false;
        switch (args[0])
        {
            case "set_menu":
                if (args.Length > 1)
                {
                    if (UnpackAliasToMenuList(args[1]))
                        return true;
                    Print("set menu error alias not found");
                    return true;
                }
                Print("set menu error");
                return true;
            case "all_binds":
                PrintBinds();
                return true;
            case "all_aliases":
                PrintAliases();
                return true;
            case "max_strings":
                int tempValue;
                if (args.Length == 2 && int.TryParse(args[1], out tempValue))
                    _maxStrings = Mathf.Max(10, tempValue);
                return true;
            case "max_visible_strings":
                int tempValue2;
                if (args.Length == 2 && int.TryParse(args[1], out tempValue2))
                {
                    _maxVisibleStrings = Mathf.Max(10, tempValue2);
                    windowRect.Set(0, 0, Screen.width, _maxVisibleStrings * 15.625f + 50);
                }
                return true;
            case "bind":
                if (args.Length == 3)
                {
                    if (args[1] != "" && args[2] != "")
                    {
                        if (_binds.ContainsKey(args[1]))
                        {
                            _binds[args[1]] = args[2];
                        }
                        else
                        {
                            _binds.Add(args[1], args[2]);
                        }
                        return true;
                    }
                }
                Print("set bind error");
                return true;
            case "alias":
                if (args.Length >= 3)
                {
                    if (args[1] != "")
                    {
                        string[] value = new string[args.Length - 2];
                        for (int i = 2; i < args.Length; i++)
                        {
                            value[i - 2] = args[i];
                        }
                        if (_aliases.ContainsKey(args[1]))
                        {
                            _aliases[args[1]] = value;
                        }
                        else
                        {
                            _aliases.Add(args[1], value);
                        }
                        return true;
                    }
                }
                else
                {
                    if (args.Length == 2)
                    {
                        _aliases[args[1]] = new string[0];
                        return true;
                    }
                }
                Print("set alias error");
                return true;
            case "debug":
                if (args.Length > 1)
                {
                    if (args[1] != "")
                    {
                        _debug = args[1] != "0";
                        return true;
                    }
                }
                else
                {
                    Print("debug " + (_debug ? 1 : 0));
                }
                return true;
            case "binds_on":
                if (args.Length > 1)
                {
                    if (args[1] != "")
                    {
                        _bindsOn = args[1] != "0";
                        return true;
                    }
                }
                else
                {
                    _bindsOn = !_bindsOn;
                    Print("binds_on " + (_bindsOn ? 1 : 0));
                }
                return true;
            case "exec_bind":
                if (args.Length == 2)
                {
                    string value;
                    if (_binds.TryGetValue(args[1],out value))
                    {
                        Submit(value, false, 0);
                        return true;
                    }
                }
                Print("bind not found");
                return true;
            default:
                string[] args2;
                if (_aliases.TryGetValue(args[0], out args2))
                {
                    if (args2.Length > 0)
                    {
                        float wait = 0;
                        for (int i = 0; i < args2.Length; i++)
                        {
                            float currerntWait = 0;
                            if (args2[i].Contains("wait"))
                            {
                                currerntWait = GetWait(LineReader(args2[i]));
                            }
                            if (currerntWait > 0)
                            {
                                wait += currerntWait;
                                i++;
                                if (i == args2.Length)
                                    break;
                            }

                            int ii = i;
                            Timer.CreateTimer(wait, () =>
                            {
                                Submit(args2[ii], true, ++callsCount);
                            });
                        }
                        return true;
                    }
                }
                break;
        }
        if (_debug)
            Print("\"" + args[0] + "\"" + " unknown command");
        return false;
    }

    float GetWait(string[] args)
    {
        if (args.Length == 0)
            return 0;
        if (args[0] == "wait")
        {
            int miliseconds = 0;
            if (args.Length == 2 && int.TryParse(args[1], out miliseconds))
            {
                return miliseconds / 1000f;
            }
            return 0.5f;
        }
        return 0;
    }

    public void Print(string text)
    {
        _consoleText.Add(text);
        _consoleTextTransperent.Add(text);
        scrollValue = 0;
        if (_consoleTextTransperent.Count == 1)
        {
            _consoleTextTransperentTime = Time.time + _consoleTextTransperentDelay;
        }
        if (_consoleText.Count > _maxStrings)
        {
            _consoleText.RemoveAt(0);
        }
    }

    public class Timer
    {
        public Action Func;
        public float EndTime;
        public Timer(Action func, float endTime)
        {
            Func = func;
            EndTime = endTime;
        }

        public void Stop()
        {
            timers.Remove(this);
        }

        public static void CreateTimer(float delay, Action func)
        {
            timers.Add(new Timer(func, delay + Time.time));
        }

        public static void Update()
        {
            for (int i = 0; i < timers.Count; i++)
            {
                var timer = timers[i];
                if (timer.EndTime < Time.time)
                {
                    timer.Func();
                    timer.Stop();
                    i--;
                }
            }
        }

        static List<Timer> timers = new List<Timer>();
    }
}
